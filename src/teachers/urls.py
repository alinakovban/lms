from django.urls import path
from teachers.views import get_teacher, create_teacher

urlpatterns = [
    path('', get_teacher, name='get_teacher'),
    path('create/', create_teacher, name='create_teacher')
]
