from django.db import models
from faker import Faker

import random
import string


class Teacher(models.Model):
    group = models.CharField(max_length=120, null=True, blank=True)
    subject = models.CharField(max_length=120, null=True, blank=True)
    average_grade = models.PositiveSmallIntegerField(null=True, blank=True)
    teachers_name = models.CharField(max_length=120, null=True, blank=True)
    day_of_the_lesson = models.CharField(max_length=120, null=True, blank=True)
    time_of_the_lesson = models.TimeField(null=True, blank=True)

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                group=''.join(random.choices(string.ascii_letters, k=2)),
                subject=faker.random_element(['Mathematics', 'Computer Science']),
                average_grade=faker.pydecimal(min_value=50, max_value=100),
                teachers_name=faker.first_name() + ' ' + faker.last_name(),
                day_of_the_lesson=faker.random_element(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']),
                time_of_the_lesson=faker.time()
            )

    def __str__(self):
        return f'{self.group} {self.teachers_name} {self.subject} {self.pk}'
