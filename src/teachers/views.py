from django.http import HttpResponse, HttpResponseRedirect
from .models import Teacher
from config.utils import format_records
from webargs.djangoparser import use_args
from webargs import fields
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt


def index(request):
    return HttpResponse("Hello teachers")


@use_args(
    {
        "teachers_name": fields.Str(
            required=False
        ),
        "subject": fields.Str(
            required=False
        ),
        "search": fields.Str(
            required=False
        ),
    },
    location="query"
)
def get_teacher(request, params):
    teachers = Teacher.objects.all()

    search_fields = ["teachers_name", "subject", "group"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": param_value})
            teachers = teachers.filter(or_filter)
        else:
            teachers = teachers.filter(**{param_name: param_value})

    return HttpResponse(format_records(teachers))


@csrf_exempt
def create_teacher(request):
    form = """

    <form method='post'>
      <label for="groupid">Group:</label><br>
      <input type="text" id="groupid" name="group" placeholder="Group"><br>

      <label for="teachersNameid">Teachers name:</label><br>
      <input type="text" id="teachersNameid" name="teachers_name"><br><br>

      <label for="subjectid">Subject:</label><br>
      <input type="subject" id="subject" name="subject"><br><br>

      <input type="submit" value="Send">
    </form>"""

    if request.method == 'POST':
        teachers = Teacher.objects.create(
            teachers_name=request.POST.get('teachers_name'),
            subject=request.POST.get('subject'),
            group=request.POST.get('group')
        )
        return HttpResponseRedirect('/teachers')
    elif request.method == 'GET':
        pass

    return HttpResponse(form)
