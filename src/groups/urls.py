from django.urls import path
from groups.views import get_group, create_group

urlpatterns = [
    path('', get_group, name='get_group'),
    path('create/', create_group, name='create_group')
]
