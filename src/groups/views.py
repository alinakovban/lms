from django.http import HttpResponse, HttpResponseRedirect
from .models import Group
from config.utils import format_records
from webargs.djangoparser import use_args
from webargs import fields
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt


def index(request):
    return HttpResponse("Hello group")


@use_args(
    {
        "name_group": fields.Str(
            required=False
        ),
        "specialization": fields.Str(
            required=False
        ),
        "search": fields.Str(
            required=False
        ),
    },
    location="query"
)
def get_group(request, params):
    groups = Group.objects.all()

    search_fields = ["name_group", "specialization", "teachers_name"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": param_value})
            groups = groups.filter(or_filter)
        else:
            groups = groups.filter(**{param_name: param_value})

    return HttpResponse(format_records(groups))


@csrf_exempt
def create_group(request):
    form = """

    <form method='post'>
      <label for="nameGroupid">Group name:</label><br>
      <input type="text" id="nameGroupid" name="name_group" placeholder="Group name"><br>

      <label for="teachersNameid">Teachers name:</label><br>
      <input type="text" id="teachersNameid" name="teachers_name"><br><br>

      <label for="specializationid">Specialization:</label><br>
      <input type="specialization" id="specializationid" name="specialization"><br><br>

      <input type="submit" value="Send">
    </form>"""

    if request.method == 'POST':
        groups = Group.objects.create(
            name_group=request.POST.get('name_group'),
            specialization=request.POST.get('specialization'),
            teachers_name=request.POST.get('teachers_name')
        )
        return HttpResponseRedirect('/groups')
    elif request.method == 'GET':
        pass

    return HttpResponse(form)
