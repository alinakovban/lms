from django.db import models
from faker import Faker

import random
import string


class Group(models.Model):
    name_group = models.CharField(max_length=120, null=True, blank=True)
    head_of_the_group = models.CharField(max_length=120, null=True, blank=True)
    quantity_of_students = models.PositiveSmallIntegerField(null=True,
                                                            blank=True)
    teachers_name = models.CharField(max_length=120, null=True, blank=True)
    specialization = models.CharField(max_length=120, null=True, blank=True)
    average_grade = models.PositiveSmallIntegerField(null=True, blank=True)

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                name_group=''.join(random.choices(string.ascii_letters, k=2)),
                head_of_the_group=faker.first_name() + ' ' + faker.last_name(),
                quantity_of_students=faker.pydecimal(min_value=10,
                                                     max_value=20),
                teachers_name=faker.first_name() + ' ' + faker.last_name(),
                specialization=faker.random_element(['Mathematics',
                                                     'Computer Science']),
                average_grade=faker.pydecimal(min_value=50, max_value=100),
            )

    def __str__(self):
        return f'{self.name_group} {self.teachers_name} {self.specialization} {self.pk}'
