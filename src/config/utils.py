def format_records(records: list):
    return '<br>'.join(str(record) for record in records)
