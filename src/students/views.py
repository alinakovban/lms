from django.http import HttpResponse, HttpResponseRedirect
from .models import Student
from config.utils import format_records
from webargs.djangoparser import use_args
from webargs import fields
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt


def index(request):
    students = Student.objects.all()
    output = ', '.join([student.first_name + ' ' + student.last_name for student in students])
    return HttpResponse(output)


@use_args(
    {
        "first_name": fields.Str(
            required=False
        ),
        "last_name": fields.Str(
            required=False
        ),
        "search": fields.Str(
            required=False
        ),
    },
    location="query"
)
def get_students(request, params):
    students = Student.objects.all()

    search_fields = ["first_name", "last_name", "email"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": param_value})
            students = students.filter(or_filter)
        else:
            students = students.filter(**{param_name: param_value})

    return HttpResponse(format_records(students))


@csrf_exempt
def create_student(request):
    form = """

    <form method='post'>
      <label for="firstNameid">First name:</label><br>
      <input type="text" id="firstNameid" name="first_name" placeholder="First name"><br>

      <label for="lastNameid">Last name:</label><br>
      <input type="text" id="lastNameid" name="last_name"><br><br>

      <label for="emailid">Email:</label><br>
      <input type="email" id="emailid" name="email"><br><br>

      <input type="submit" value="Send">
    </form>"""

    if request.method == 'POST':
        student = Student.objects.create(
            first_name=request.POST.get('first_name'),
            last_name=request.POST.get('last_name'),
            email=request.POST.get('email')
        )
        return HttpResponseRedirect('/students')
    elif request.method == 'GET':
        pass

    return HttpResponse(form)
