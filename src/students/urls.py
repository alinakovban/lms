from django.urls import path
from students.views import get_students, create_student

urlpatterns = [
    path('', get_students, name='get_students'),
    path('create/', create_student, name='create_student')
]
